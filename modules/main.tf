terraform {
  required_version = ">= 1.0"

  required_providers {
    aws = {
      source = "hashicorp/aws"
    }
  }
}

locals {
  secret_name = var.not_retailer ? "${var.retailer}_SFTP_${var.environment}" : "cf_ravel_${var.retailer}"
  home_directory = var.directory_change ? "${var.sftp_bucket}" : "${var.sftp_bucket}/${var.retailer}"
}

data "aws_iam_policy_document" "transfer_policy" {
  statement {
    actions   = [
                "s3:ListBucket",
                "s3:GetBucketLocation"
                ]
    resources = [
                "arn:aws:s3:::${var.sftp_bucket}"
                ]
  }
  statement {
      actions = [
                "s3:PutObject",
                "s3:GetObject",
                "s3:DeleteObject",
                "s3:DeleteObjectVersion",
                "s3:GetObjectVersion",
                "s3:GetObjectACL",
                "s3:PutObjectACL"
      ]
      resources = [
                "arn:aws:s3:::${local.home_directory}/*"
      ]
  }
}

data "aws_iam_policy_document" "assume_externalid" {
    statement {
        actions = [
            "sts:AssumeRole"
        ]
        principals {
            type        = "Service"
            identifiers = ["transfer.amazonaws.com"]
        } 
    }
}

resource "aws_iam_policy" "policy" {

  name        = "sftp-${var.retailer}-policy"
  path        = "/"
  description = "SFTP policy for retailer"

  policy = data.aws_iam_policy_document.transfer_policy.json
}

resource "aws_iam_role" "retailer_role" {

  name                 = "sftp-role-${var.retailer}"
  path                 = "/"
  max_session_duration = 3600
  description          = "Role to enable Retailer to use AWS Transfer"
  assume_role_policy   = data.aws_iam_policy_document.assume_externalid.json
}

resource "aws_iam_role_policy_attachment" "attach_policy" {

  role       = aws_iam_role.retailer_role.name
  policy_arn = aws_iam_policy.policy.arn
}

resource "aws_secretsmanager_secret" "retailer" {
  name = "SFTP/${local.secret_name}"
}

resource "random_password" "password" {
  length = 16
  special = true
  override_special = "_%@"
}


resource "aws_secretsmanager_secret_version" "example" {
  secret_id     = aws_secretsmanager_secret.retailer.id
  secret_string = jsonencode(
          {"Role" = aws_iam_role.retailer_role.arn,
          "Password" = random_password.password.result,
          "HomeDirectory" = "/${local.home_directory}"
          })
}