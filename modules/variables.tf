variable "sftp_bucket" {
    type = string
    default = "cf-ravel-fileserver"
    description = "S3 bucket that's hosting the SFTP. Change if it's different from default"
}

variable "retailer" {
    type = string
    description = "Retailer that will be added on the SFTP server"
}

variable "not_retailer" {
    default = false
    description = "Retailer that will be added on the SFTP server"
}

variable "environment" {
    type = string
    default = "prod"
    description = "Environment"
}

variable "directory_change" {
    type = bool
    default = false
    description = "Define if directory needs to be changed"
}