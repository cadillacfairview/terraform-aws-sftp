terraform {
  required_version = ">= 1.0"

  required_providers {
    aws = {
      source = "hashicorp/aws"
    }
  }
}

locals {
  secret_name = var.user
}

data "aws_iam_policy_document" "transfer_policy" {
  count = var.write_privilege ? 1 : 0

  statement {
    actions   = [
                "s3:ListBucket",
                "s3:GetBucketLocation"
                ]
    resources = [
                "arn:aws:s3:::${var.sftp_bucket}"
                ]
  }
  statement {
      actions = [
                "s3:PutObject",
                "s3:GetObject",
                "s3:DeleteObject",
                "s3:DeleteObjectVersion",
                "s3:GetObjectVersion",
                "s3:GetObjectACL",
                "s3:PutObjectACL"
      ]
      resources = [
                "arn:aws:s3:::${var.sftp_bucket}/inbound/*",
                "arn:aws:s3:::${var.sftp_bucket}/outbound/*"
      ]
  }
}

data "aws_iam_policy_document" "transfer_policy_read" {
  count = var.write_privilege ? 0 : 1
  
  statement {
    actions   = [
                "s3:ListBucket",
                "s3:GetBucketLocation"
                ]
    resources = [
                "arn:aws:s3:::${var.sftp_bucket}"
                ]
  }
  statement {
      actions = [
                "s3:GetObject",
                "s3:GetObjectVersion",
                "s3:GetObjectACL",
      ]
      resources = [
                "arn:aws:s3:::${var.sftp_bucket}/inbound/*",
                "arn:aws:s3:::${var.sftp_bucket}/outbound/*"
                
      ]
  }
}

data "aws_iam_policy_document" "assume_externalid" {
    statement {
        actions = [
            "sts:AssumeRole"
        ]
        principals {
            type        = "Service"
            identifiers = ["transfer.amazonaws.com"]
        } 
    }
}

resource "aws_iam_policy" "policy" {

  name        = "sftp-${var.user}-policy"
  path        = "/"
  description = "SFTP policy for User"

  policy = var.write_privilege ? data.aws_iam_policy_document.transfer_policy[0].json : data.aws_iam_policy_document.transfer_policy_read[0].json
}

resource "aws_iam_role" "user_role" {

  name                 = "sftp-role-${var.user}"
  path                 = "/"
  max_session_duration = 3600
  description          = "Role to enable user to use AWS Transfer"
  assume_role_policy   = data.aws_iam_policy_document.assume_externalid.json
}

resource "aws_iam_role_policy_attachment" "attach_policy" {

  role       = aws_iam_role.user_role.name
  policy_arn = aws_iam_policy.policy.arn
}

resource "aws_secretsmanager_secret" "user" {
  name = "SFTP/${local.secret_name}"
}

resource "random_password" "password" {
  length = 16
  special = true
  override_special = "_%@"
}


resource "aws_secretsmanager_secret_version" "example" {
  secret_id     = aws_secretsmanager_secret.user.id
  secret_string = jsonencode(
          {"Role" = aws_iam_role.user_role.arn,
          "Password" = random_password.password.result,
          "HomeDirectory" = "/${var.sftp_bucket}"
          })
}