variable "sftp_bucket" {
    type = string
    default = "cf-sftp-bucket-acs"
    description = "S3 bucket that's hosting the SFTP. Change if it's different from default"
}

variable "user" {
    type = string
    description = "Company that will be added on the SFTP server"
}

variable "environment" {
    type = string
    default = "prod"
    description = "Environment"
}

variable "write_privilege" {
    default = true
    description = "Write privileges for FTP"
}