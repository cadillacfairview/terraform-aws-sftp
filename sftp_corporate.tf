### SFTP for Corporate
# module "sftp_corporate_nonprod" {
#     source = "./modules"
#     retailer = "corporate"
#     sftp_bucket = "cf-sftp-corporate-nonprod"
#     not_retailer = true
#     directory_change = true
#     environment = "nonprod"
#     providers = {
#         aws = aws.data-platform-non-prod
#     }
# }

module "sftp_corporate_prod" {
    source = "./modules"
    retailer = "corporate"
    sftp_bucket = "cf-sftp-corporate-prod"
    not_retailer = true
    directory_change = true
    providers = {
        aws = aws.data-platform-prod
    }
}