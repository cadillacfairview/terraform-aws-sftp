### SFTP for ENVIRONICS
module "sftp_acs" {
    source = "./modules/sftp_adobe"
    user = "sftp_cf"
    sftp_bucket = "cf-sftp-bucket-acs"
    environment = "prod"
    providers = {
        aws = aws.data-platform-prod
    }
}

module "sftp_wtd_acs" {
    source = "./modules/sftp_adobe"
    user = "sftp_wtd"
    sftp_bucket = "cf-sftp-bucket-acs"
    environment = "prod"
    providers = {
        aws = aws.data-platform-prod
    }
}

module "sftp_north_acs" {
    source = "./modules/sftp_adobe"
    user = "sftp_north"
    write_privilege = false
    sftp_bucket = "cf-sftp-bucket-acs"
    environment = "prod"
    providers = {
        aws = aws.data-platform-prod
    }
}