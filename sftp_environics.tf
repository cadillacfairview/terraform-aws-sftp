### SFTP for ENVIRONICS
# module "sftp_environics" {
#     source = "./modules"
#     retailer = "environics"
#     sftp_bucket = "environics-mobilescapes-nonprod"
#     not_retailer = true
#     environment = "nonprod"
#     providers = {
#         aws = aws.data-platform-non-prod
#     }
# }

module "sftp_environics_prod" {
    source = "./modules"
    retailer = "environics"
    sftp_bucket = "environics-mobilescapes-prod"
    not_retailer = true
    providers = {
        aws = aws.data-platform-prod
    }
}