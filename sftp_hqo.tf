### SFTP for ENVIRONICS
# module "sftp_hqo" {
#     source = "./modules"
#     retailer = "hqo"
#     sftp_bucket = "cf-concierge-kpi-nonprod"
#     not_retailer = true
#     environment = "nonprod"
#     providers = {
#         aws = aws.data-platform-non-prod
#     }
# }

module "sftp_hqo_prod" {
    source = "./modules"
    retailer = "hqo"
    sftp_bucket = "cf-concierge-kpi-prod"
    not_retailer = true
    providers = {
        aws = aws.data-platform-prod
    }
}